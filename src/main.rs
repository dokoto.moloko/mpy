use std::process;
use hyper::body::Buf;
use hyper_tls::HttpsConnector;
use hyper::{ Client, Uri };
use serde::Deserialize;
use regex::Regex;
use clap::{ Arg, App };

#[derive(Deserialize, Debug)]
struct Monitor {
    name: String,
}

#[derive(Deserialize, Debug)]
struct Psp {
    monitors: Vec<Monitor>,
}

#[derive(Deserialize, Debug)]
struct InvInstances {
    psp: Psp,
}

const INVI_EXCLUDES: [&str; 2] = ["api.invidious.io", "docs.invidious.io"];
const RX_YT_VIDEO_ID: &str = r#"(?:https?://)?(?:www\.)?youtu(?:be)?\.(?:com|be)(?:/watch/?\?v=|/embed/|/shorts/|/)(\w+)"#;
type ResultTokio<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

#[tokio::main]
async fn main() -> ResultTokio<()> {
    let args_matches = App::new("yt2inv")
        .version("0.1.0")
        .about("Convert youtube url to invidius url")
        .author("Dokoto <trabajo@dkt-mlk.es>")
        .arg(Arg::with_name("url")
          .help("Youtube url to convert")
          .short("u")
          .long("url")
          .required(true)
          .takes_value(true),
        ).get_matches();

    let yt_url = match args_matches.value_of("url") {
        Some(url) => url,
        None => {
            println!("Url param not found");
            process::exit(-1);
        }
    };
    let yt_video_id = match parse_yt_url(yt_url) {
        Ok(id) => id,
        Err(e) => {
            println!("{}", e);
            process::exit(-1);
        }
    };
    let inv_instances = fetch_inv_instances().await?;
    let inv_instance: Vec<&Monitor> = inv_instances.psp.monitors
        .iter()
        .filter(|monitor| INVI_EXCLUDES.iter().find(|&&i| i == monitor.name.as_str()).is_none())
        .take(1)
        .collect(); 
    let url: &str = &inv_instance[0].name;
    println!("https://{}/watch?v={}", url, yt_video_id);
    Ok(())
}

fn parse_yt_url(url: &str) -> Result<String, String> {
    match Regex::new(RX_YT_VIDEO_ID) {
        Ok(rx) => {
            match rx.captures(url) {
                Some(cap) => Ok(String::from(&cap[1])),
                None => Err(format!("No youtube video id found in {}", url)),
            }
        }
        Err(e) => Err(format!("Regex expresion on error number {}", e)),
    }
}

async fn fetch_inv_instances() -> ResultTokio<InvInstances> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);
    let url = "https://stats.uptimerobot.com/api/getMonitorList/89VnzSKAn?page=1";
    let res = client.get(Uri::from_static(url)).await?;
    let body = hyper::body::aggregate(res).await?;
    let inv_instances = serde_json::from_reader(body.reader())?;
    Ok(inv_instances)
}
